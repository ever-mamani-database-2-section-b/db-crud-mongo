package university.jala;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import university.jala.conexionDB.Estudiante;
import university.jala.conexionDB.EstudianteDAO;
import university.jala.conexionDB.MongoDBException;
import university.jala.conexionDB.EstudiantePrinter;

/*
 * La clase Main es la clase principal del programa.
 */
public class Main {

  public static void main(String[] args) throws MongoDBException {

    EstudianteDAO estudianteDAO = new EstudianteDAO();

    // SELECT
    System.out.printf("Estudiantes en la base de datos:%n");
    List<Estudiante> losEstudiantes = estudianteDAO.obtenerTodosLosEstudiantes();
    EstudiantePrinter.printEstudiantesInJSONFormat(losEstudiantes);

    // INSERT
    Estudiante nuevoEstudiante = new Estudiante(null, "Ana García", "ana.garcia@example.com", 21,
        "Femenino", "456 Avenida del Sol", "Ciudad Alegre", "123-456-7890",
        Arrays.asList("Matemáticas", "Historia", "Ciencias"),
        Map.of("matematicas", 95, "historia", 88, "ciencias", 92));

    estudianteDAO.insertarEstudiante(nuevoEstudiante);

    // UPDATE
    String estudianteId = "65109795afcb8d5f10fdc849";
    Estudiante estudianteEncontrado = estudianteDAO.obtenerEstudiantePorId(estudianteId);

    estudianteEncontrado.setNombre("Perla García Ponce");
    estudianteEncontrado.setEdad(22);
    estudianteEncontrado.setCiudad("Santa Cruz");
    estudianteEncontrado.setTelefono("123-456-7890");
    estudianteEncontrado.setCorreo("perla.garcia.ponce@example.com");
    estudianteDAO.actualizarEstudiante(estudianteId, estudianteEncontrado);
    System.out.printf("Estudiante con ID %s actualizado%n", estudianteId);

    // DELETE
    estudianteDAO.eliminarEstudiante("6510c0a1d417b5de7e183e5c");
    System.out.printf("Estudiante con ID %s eliminado%n", estudianteId);

    // SELECT
    System.out.printf("Estudiantes en la base de datos:%n");
    List<Estudiante> todosLosEstudiantes = estudianteDAO.obtenerTodosLosEstudiantes();
    EstudiantePrinter.printEstudiantesInJSONFormat(todosLosEstudiantes);
  }
}
use('Estudiante');

db.estudiantes.insertOne({
  _id: ObjectId("65109795afcb8d5f10fdc849"),
  nombre: "Ana García",
  correo: "ana.garcia@example.com",
  edad: 21,
  genero: "Femenino",
  direccion: "456 Avenida del Sol",
  ciudad: "Ciudad Alegre",
  telefono: "123-456-7890",
  materias: ["Matemáticas", "Historia", "Ciencias"],
  notas: {
    matematicas: 95, historia: 88, ciencias: 92
  }
})

db.estudiantes.insertMany([{
  nombre: "María López",
  correo: "maria.lopez@example.com",
  edad: 20,
  genero: "Femenino",
  direccion: "321 Calle de la Luna",
  ciudad: "Ciudad Serena",
  telefono: "987-654-3210",
  materias: ["Geografía", "Literatura", "Arte"],
  notas: {
    geografia: 92, literatura: 85, arte: 78
  }
}, {
  nombre: "Carlos Rodríguez",
  correo: "carlos.rodriguez@example.com",
  edad: 23,
  genero: "Masculino",
  direccion: "234 Avenida del Mar",
  ciudad: "Ciudad Costera",
  telefono: "777-777-7777",
  materias: ["Historia", "Música", "Educación Física"],
  notas: {
    historia: 88, musica: 75, educacion_fisica: 92
  }
}, {
  _id: ObjectId("6510c0a1d417b5de7e183e5c"),
  nombre: "Laura Martínez",
  correo: "laura.martinez@example.com",
  edad: 19,
  genero: "Femenino",
  direccion: "567 Calle del Río",
  ciudad: "Ciudad Fluvial",
  telefono: "444-444-4444",
  materias: ["Química", "Biología", "Matemáticas"],
  notas: {
    quimica: 89, biologia: 94, matematicas: 91
  }
}, {
  nombre: "Luis Sánchez",
  correo: "luis.sanchez@example.com",
  edad: 24,
  genero: "Masculino",
  direccion: "876 Avenida de la Montaña",
  ciudad: "Ciudad de la Montaña",
  telefono: "888-888-8888",
  materias: ["Física", "Química", "Informática"],
  notas: {
    fisica: 85, quimica: 90, informatica: 88
  }
}])
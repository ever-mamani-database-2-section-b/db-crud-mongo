package university.jala.conexionDB;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import java.util.HashMap;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * La clase EstudianteDAO se encarga de interactuar con la base de datos MongoDB para realizar
 * operaciones CRUD en los documentos de estudiantes.
 *
 * @author Ever Mamani Vicente..
 */
public class EstudianteDAO {

  private static final String COLECCION_ESTUDIANTES = "estudiantes";
  private static final String ID = "_id";
  private static final String NOMBRE = "nombre";
  private static final String CORREO = "correo";
  private static final String EDAD = "edad";
  private static final String GENERO = "genero";
  private static final String DIRECCION = "direccion";
  private static final String CIUDAD = "ciudad";
  private static final String TELEFONO = "telefono";
  private static final String MATERIAS = "materias";
  private static final String NOTAS = "notas";
  private final MongoCollection<Document> collection;

  /**
   * Constructor de la clase EstudianteDAO que establece la conexión a la base de datos.
   *
   * @throws MongoDBException Si ocurre un error al conectar con MongoDB.
   */
  public EstudianteDAO() throws MongoDBException {
    MongoDBConnection mongoDBConnection = MongoDBConnection.getInstance();
    collection = mongoDBConnection.getDatabase().getCollection(COLECCION_ESTUDIANTES);
  }

  /**
   * Método para insertar un nuevo estudiante en la base de datos.
   *
   * @param estudiante El objeto Estudiante que se desea insertar.
   * @throws MongoDBException Si ocurre un error durante la inserción.
   */
  public void insertarEstudiante(Estudiante estudiante) throws MongoDBException {
    try {
      Document estudianteDoc = convertirEstudianteADocumento(estudiante);
      collection.insertOne(estudianteDoc);
    } catch (Exception e) {
      throw new MongoDBException("Error al insertar estudiante");
    }
  }

  /**
   * Método para actualizar los datos de un estudiante en la base de datos.
   *
   * @param id         El ID del estudiante que se desea actualizar.
   * @param estudiante El objeto Estudiante con los nuevos datos.
   * @throws MongoDBException Si ocurre un error durante la actualización.
   */
  public void actualizarEstudiante(String id, Estudiante estudiante) throws MongoDBException {
    try {
      ObjectId objectId = new ObjectId(id);
      Bson filter = Filters.eq(ID, objectId);
      Bson update = Updates.combine(Updates.set(NOMBRE, estudiante.getNombre()),
          Updates.set(CORREO, estudiante.getCorreo()), Updates.set(EDAD, estudiante.getEdad()),
          Updates.set(GENERO, estudiante.getGenero()),
          Updates.set(DIRECCION, estudiante.getDireccion()),
          Updates.set(CIUDAD, estudiante.getCiudad()),
          Updates.set(TELEFONO, estudiante.getTelefono()),
          Updates.set(MATERIAS, estudiante.getMaterias()),
          Updates.set(NOTAS, estudiante.getNotas()));

      UpdateResult result = collection.updateOne(filter, update);

      if (result.getModifiedCount() != 1) {
        throw new MongoDBException("No se pudo actualizar el estudiante con ID: " + id);
      }
    } catch (Exception e) {
      throw new MongoDBException("Error al actualizar estudiante");
    }
  }

  /**
   * Elimina un estudiante de la base de datos por su ID.
   *
   * @param id El ID del estudiante que se va a eliminar.
   * @throws MongoDBException Si ocurre un error durante la eliminación o si no se encuentra el
   *                          estudiante con el ID proporcionado.
   */
  public void eliminarEstudiante(String id) throws MongoDBException {
    try {
      ObjectId objectId = new ObjectId(id);
      Bson filter = Filters.eq(ID, objectId);
      DeleteResult result = collection.deleteOne(filter);

      if (result.getDeletedCount() != 1) {
        throw new MongoDBException("No se pudo eliminar el estudiante con ID: " + id);
      }
    } catch (Exception e) {
      throw new MongoDBException("Error al eliminar estudiante");
    }
  }

  /**
   * Obtiene un estudiante de la base de datos por su ID.
   *
   * @param id El ID del estudiante que se desea obtener.
   * @return El objeto Estudiante correspondiente al ID proporcionado.
   * @throws MongoDBException Si ocurre un error durante la búsqueda o si no se encuentra el
   *                          estudiante con el ID proporcionado.
   */
  public Estudiante obtenerEstudiantePorId(String id) throws MongoDBException {
    try {
      ObjectId objectId = new ObjectId(id);
      Bson filter = Filters.eq(ID, objectId);
      Document doc = collection.find(filter).first();

      if (doc != null) {
        return convertirDocumentoAEstudiante(doc);
      } else {
        throw new MongoDBException("No se encontró el estudiante con ID: " + id);
      }
    } catch (IllegalArgumentException e) {
      throw new MongoDBException("ID no válido: " + id);
    } catch (Exception e) {
      throw new MongoDBException("Error al obtener estudiante por ID");
    }
  }

  /**
   * Obtiene todos los estudiantes almacenados en la base de datos.
   *
   * @return Una lista de objetos Estudiante que representa a todos los estudiantes.
   * @throws MongoDBException Si ocurre un error durante la búsqueda de los estudiantes.
   */
  public List<Estudiante> obtenerTodosLosEstudiantes() throws MongoDBException {
    try {
      List<Estudiante> estudiantes = new ArrayList<>();
      MongoCursor<Document> cursor = collection.find().iterator();

      while (cursor.hasNext()) {
        Document doc = cursor.next();
        estudiantes.add(convertirDocumentoAEstudiante(doc));
      }

      return estudiantes;
    } catch (Exception e) {
      throw new MongoDBException("Error al obtener todos los estudiantes");
    }
  }

  private Document convertirEstudianteADocumento(Estudiante estudiante) {
    return new Document().append(NOMBRE, estudiante.getNombre())
        .append(CORREO, estudiante.getCorreo()).append(EDAD, estudiante.getEdad())
        .append(GENERO, estudiante.getGenero()).append(DIRECCION, estudiante.getDireccion())
        .append(CIUDAD, estudiante.getCiudad()).append(TELEFONO, estudiante.getTelefono())
        .append(MATERIAS, estudiante.getMaterias()).append(NOTAS, estudiante.getNotas());
  }

  private Estudiante convertirDocumentoAEstudiante(Document doc) {
    String id = doc.getObjectId(ID).toString();
    String nombre = doc.getString(NOMBRE);
    String correo = doc.getString(CORREO);
    int edad = doc.getInteger(EDAD);
    String genero = doc.getString(GENERO);
    String direccion = doc.getString(DIRECCION);
    String ciudad = doc.getString(CIUDAD);
    String telefono = doc.getString(TELEFONO);

    List<String> materias = doc.getList(MATERIAS, String.class);

    Document notasDocument = doc.get(NOTAS, Document.class);
    Map<String, Integer> notas = new HashMap<>();
    for (String key : notasDocument.keySet()) {
      notas.put(key, notasDocument.getInteger(key));
    }

    return new Estudiante(id, nombre, correo, edad, genero, direccion, ciudad, telefono, materias,
        notas);
  }
}

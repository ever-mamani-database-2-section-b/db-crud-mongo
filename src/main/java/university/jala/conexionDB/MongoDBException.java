package university.jala.conexionDB;

/**
 * La clase MongoDBException representa una excepción que ocurre al conectar con MongoDB.
 *
 * @author Ever Mamani Vicente..
 */
public class MongoDBException extends Exception {

  /**
   * Constructor de la clase MongoDBException.
   *
   * @param message El mensaje de error.
   */
  public MongoDBException(String message) {
    super(message);
  }
}

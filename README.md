# Proyecto CRUD de Estudiantes con MongoDB y Java

Este proyecto Java demuestra un ejemplo de operaciones CRUD (Crear, Leer, Actualizar, Eliminar) en una base de datos MongoDB utilizando Java y la biblioteca MongoDB Java Driver. El proyecto realiza estas operaciones en una colección de estudiantes almacenada en una base de datos MongoDB.

## Estructura del Proyecto

El proyecto se divide en varias clases principales:

### `Estudiante`

**Descripción**: La clase `Estudiante` representa a un estudiante y se utiliza para crear objetos de estudiante con información relevante.

**Constructores**:
- `Estudiante(String id, String nombre, String correo, int edad, String genero, String direccion, String ciudad, String telefono, List<String> materias, Map<String, Integer> notas)`: Constructor para crear un nuevo objeto Estudiante con información detallada.

### `EstudianteDAO`

**Descripción**: La clase `EstudianteDAO` se encarga de realizar operaciones CRUD en una colección de estudiantes en la base de datos MongoDB.

**Métodos**:
- `insertarEstudiante(Estudiante estudiante)`: Inserta un nuevo estudiante en la base de datos.
- `actualizarEstudiante(String id, Estudiante estudiante)`: Actualiza la información de un estudiante existente en la base de datos.
- `eliminarEstudiante(String id)`: Elimina un estudiante de la base de datos por su ID.
- `obtenerEstudiantePorId(String id)`: Obtiene un estudiante de la base de datos por su ID.
- `obtenerTodosLosEstudiantes()`: Obtiene todos los estudiantes de la base de datos y los devuelve como una lista.

### `EstudiantePrinter`

**Descripción**: La clase `EstudiantePrinter` proporciona una utilidad para imprimir la lista de estudiantes en formato JSON en la consola.

**Métodos**:
- `printEstudiantesInJSONFormat(List<Estudiante> estudiantes)`: Imprime la lista de estudiantes en formato JSON en la consola.

### `MongoDBConnection`

**Descripción**: La clase `MongoDBConnection` se encarga de establecer la conexión con la base de datos MongoDB.

**Métodos**:
- `getInstance()`: Método estático que devuelve una instancia de conexión a la base de datos.

### `MongoDBException`

**Descripción**: La clase `MongoDBException` es una excepción personalizada que se lanza en caso de errores relacionados con MongoDB.

## Uso del Proyecto

El archivo `Main` muestra ejemplos de uso del proyecto con operaciones CRUD en estudiantes. A continuación, se muestran ejemplos de operaciones disponibles:

1. **Inserción de Estudiante**: Crea un nuevo estudiante y lo inserta en la base de datos.

2. **Actualización de Estudiante**: Actualiza la información de un estudiante existente en la base de datos.

3. **Eliminación de Estudiante**: Elimina un estudiante de la base de datos por su ID.

4. **Obtención de Estudiante por ID**: Obtiene un estudiante de la base de datos por su ID.

5. **Obtención de Todos los Estudiantes**: Obtiene todos los estudiantes de la base de datos y los muestra en formato JSON en la consola.

Este proyecto es un ejemplo práctico de cómo realizar operaciones CRUD en una base de datos NoSQL como MongoDB utilizando Java.
